def activity_selection(activities):
    print('第一步， 根据结束时间进行排序')
    activities = sorted(activities, key=lambda x: x[2])
    print(activities)
    current_time = 0
    for value in activities:
        if current_time <= value[1]:
            print('选择步骤: ', value)
            current_time = value[2]


if __name__ == '__main__':
    activities = [(0, 1, 2), (1, 5, 9), (2, 0, 6), (3, 8, 9), (4, 3, 4)]
    print('最初的活动集合: ', activities)
    activity_selection(activities)
