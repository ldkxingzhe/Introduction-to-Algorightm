def find_closest_pairt(array_a, array_b, value_x):
    closest_value = None
    index_a = None
    index_b = None
    for _index_a in range(len(array_a)):
        for _index_b in range(len(array_a)):
            sum = array_a[_index_a] + array_b[_index_b]
            if closest_value is None:
                closest_value = sum
                index_a = _index_a
                index_b = _index_b
            else:
                if sum > value_x and abs(sum - value_x) > abs(closest_value - value_x):
                    break
                if abs(sum - value_x) < abs(closest_value - value_x):
                    closest_value = sum
                    index_a = _index_a
                    index_b = _index_b

    return (index_a, index_b)


if __name__=='__main__':
    array_a = [1, 4, 5, 7]
    array_b = [10, 20, 30, 40]
    value_x = 32
    index_a, index_b = find_closest_pairt(array_a, array_b, value_x)
    print("index_a is %d(%d), index_b is %d(%d)" % (index_a, array_a[index_a], index_b, array_b[index_b]))

    array_a = [1, 4, 5, 7]
    array_b = [10, 20, 30, 40]
    value_x = 50
    index_a, index_b = find_closest_pairt(array_a, array_b, value_x)
    print("index_a is %d(%d), index_b is %d(%d)" % (index_a, array_a[index_a], index_b, array_b[index_b]))

