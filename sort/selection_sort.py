def selection_sort(array):
    min_index = 0
    for i in range(len(array)):
        min_index = i
        for j in range(i + 1, len(array)):
            if array[min_index] > array[j]:
                min_index = j
        if min_index != i:
            array[min_index], array[i] = array[i], array[min_index]

def test_selection_sort():
    array = [1, 2, 3, 4, 5, 6]
    selection_sort(array)
    print(array)
    assert 1 == array[0]
    assert 6 == array[5]

    array = [3, 2, 1, 4, 6, 5]
    selection_sort(array)
    print(array)
    assert 1 == array[0]
    assert 6 == array[5]

