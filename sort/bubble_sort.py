
def bubble_sort(array):
    for max_index in range(len(array) - 1, 0, -1):
        for index in range(max_index):
            if array[index] > array[index + 1]:
                array[index], array[index + 1] = array[index + 1], array[index]


def test_bubble_sort():
    array = [1, 2, 3, 4, 5, 6]
    bubble_sort(array)
    assert 1 == array[0]
    assert 6 == array[5]
    array = [6, 5, 4, 3, 2, 1]
    bubble_sort(array)
    assert 1 == array[0]
    assert 6 == array[5]
    array = [2, 1, 3, 5, 6, 4]
    bubble_sort(array)
    assert 1 == array[0]
    assert 6 == array[5]
