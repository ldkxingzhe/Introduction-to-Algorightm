#!/usr/bin/env python3

def count_sort(array, range_start, range_end):
    count_array = [0] * (range_end - range_start)
    for value in array:
        index = value - range_start
        count_array[index] += 1

    current_index = 0
    for index in range(len(count_array)):
        value = count_array[index]
        if value != 0:
            for _ in range(value):
                array[current_index] = index + range_start
                current_index += 1

                
def test_count_sort():
    array = [1, 2, 3, 4, 5, 6 ,7, 8, 9, 10]
    count_sort(array, 1, 11)
    assert 1 == array[0]
    assert 10 == array[9]
    assert 3 == array[2]
    array = [10, 9, 8, 7, 6, 5, 4 ,3, 2, 1]
    count_sort(array, 1, 11)
    assert 1 == array[0]
    assert 10 == array[9]
    assert 3 == array[2]
