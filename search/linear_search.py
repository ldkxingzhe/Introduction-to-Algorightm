def linear_search(array, x):
    for i in range(len(array)):
        if x == array[i]:
            return i
    return -1


def test_linear_search():
    array = [1, 3, 6, 8, 2, 3]
    assert -1 == linear_search(array, 0)
    assert 1 == linear_search(array, 3)
