#!/usr/bin/env python3

# Given a BST, transfrom it into greater sum tree
# where each node contains sum of all nodes greater
# than that node. 
#
# Using Traversal

from graphviz import Digraph


class Node:
    def __init__(self, left=None, right=None, value=None):
        self.left: Node = left
        self.right: Node = right
        self.value = value

    def _print_graph(self, dot: Digraph):
        if self.left is None and self.right is None:
            return
        _hash = str(hash(self))
        if self.left:
            left_name = str(self.left.value)
            self.left._print_graph(dot)
            dot.node(left_name)
        else:
            left_name = _hash + 'left'
            dot.node(left_name, shape='point')
        dot.edge(str(self.value), left_name)
        if self.right:
            right_name = str(self.right.value)
            self.right._print_graph(dot)
            dot.node(right_name)
        else:
            right_name = _hash + 'right'
            dot.node(right_name, shape='point')
        dot.edge(str(self.value), right_name)

    def print_graph(self):
        dot = Digraph()
        dot.node(str(self.value))
        self._print_graph(dot)
        dot.view()


def init_origin_node():
    node35 = Node(value=35)
    node40 = Node(node35, value=40)
    node15 = Node(value=15)
    node29 = Node(left=node15, right=node40, value=29)
    node1 = Node(value=1)
    node7 = Node(value=7)
    node2 = Node(left=node1, right=node7, value=2)
    node11 = Node(left=node2, right=node29, value=11)
    return node11


def transform(bst):
    current_value = bst.value
    if bst.right is not None:
        transform(bst.right)
        if bst.right.left is not None:

    pass


def print_origin_node():
    init_origin_node().print_graph()


if __name__ == '__main__':
    print_origin_node()
