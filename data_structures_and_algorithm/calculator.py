#!/usr/bin/env python3


'''
TITLE: 简单的计算器实现
DESC: 首先可以将运算式由中缀转化为后缀， 然后进行计算
'''

def inffix_2_suffix(contents):
    ''' 中缀转化为后缀  '''
    operators = []
    result = []
    operator_priority = {
        '(':  1,
        '-': 2,
    }
    for item in contents:
        pass
    return result


def suffix_cal(contents):
    ''' 计算后缀公式的结果  '''
    pass


def cal(content_str):
    content = content_str.split(' ')
    return suffix_cal(inffix_2_suffix(content))

if __name__ == '__main__':
    first = "38 + 32 * 3 - 40 * 2"
    first_result = 38 + 32 * 3 - 40 * 2
    assert first_result == cal(first)
