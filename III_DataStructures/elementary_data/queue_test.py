from .queue import queue
import pytest

def test_queue_size():
    mqueue = queue(10)
    assert(0 == mqueue.size())
    mqueue.head = 0
    mqueue.tail = 5
    assert(5 == mqueue.size())
    mqueue.head = 1
    mqueue.tail = 0
    assert(9 == mqueue.size())


def test_enqueue_dequeue():
    mqueue = queue(5)
    mqueue.enqueue(10)
    mqueue.enqueue(20)
    mqueue.enqueue(30)
    mqueue.enqueue(40)
    with pytest.raises(NameError):
        mqueue.enqueue(50)

    assert 10 == mqueue.dequeue()
    assert 20 == mqueue.dequeue()
    assert 30 == mqueue.dequeue()
    assert 40 == mqueue.dequeue()
    with pytest.raises(NameError):
        mqueue.dequeue()
