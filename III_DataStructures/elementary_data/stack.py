class stack:
    """ Data Structures for stack  """
    def __init__(self):
        self.top = 0
        self.array = []
        
    def push(self, obj):
        self.array.append(obj)
        self.top += 1

    def is_empty(self):
        return self.top == 0
        
    def pop(self):
        if self.is_empty():
            raise NameError('underflow')
        self.top -= 1
        return self.array[self.top]
