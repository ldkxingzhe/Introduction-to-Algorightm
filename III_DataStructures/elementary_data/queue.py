class queue:
    """Queue python impletation"""

    def __init__(self, queue_size = 10):
        """ queue_size The size of queue """
        self.tail = 0
        self.head = 0
        self.queue_size = queue_size
        self.array = [0] * self.queue_size


    def enqueue(self, obj):
        size = self.size()
        if size == self.queue_size - 1:
            raise NameError("Over flow")
        else:
            self.array[self.tail] = obj
            result_tail = self.tail + 1
            self.tail = result_tail if result_tail < self.queue_size else 0

    def dequeue(self):
        if self.size() == 0:
            raise NameError("Under Flow")
        else:
            result = self.array[self.head]
            result_head = self.head + 1
            self.head = result_head if result_head < self.queue_size else 0
            return result
            
    def size(self):
        result = self.tail - self.head
        return result + self.queue_size if result < 0 else result
