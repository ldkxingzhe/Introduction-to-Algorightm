from .list import list
from .list import DirectionalList

def test_list_insert():
    mlist = list()
    mlist.insert(0, 10)
    assert(10 == mlist._first_node.next_node.data)

    mlist.insert(100, 32)
    assert(32 == mlist._first_node.next_node.next_node.data)

    mlist.insert(0, "hi")
    assert("hi" == mlist._first_node.next_node.data)

    mlist.insert(2, "78")
    assert("78" == mlist._first_node.next_node.next_node.next_node.data)

    
def test_list_get():
    mlist = list()
    mlist.insert(0, 10)
    mlist.insert(0, 11)
    mlist.insert(0, 12)
    assert(10 == mlist.get(2))
    assert(11 == mlist.get(1))
    assert(12 == mlist.get(0))


def test_list_remove_size():
    mlist = list()
    mlist.insert(600, 1)
    mlist.insert(100, 2)
    mlist.insert(300, 3)
    mlist.insert(100, 4)
    mlist.insert(200, 5)

    assert(5 == mlist.size)
    
    mlist.remove(2)
    assert(4 == mlist.get(2))
    assert(1 == mlist.get(0))
    assert(4 == mlist.size)


def test_directionallist_insert():
    mlist = DirectionalList()
    mlist.insert(0, 1)
    assert(1 == mlist.size)
    assert(1 == mlist.get(0))
    assert(1 == mlist.rget(0))

    mlist.insert(90, 3)
    assert(2 == mlist.size)
    assert(3 == mlist.get(1))
    assert(3 == mlist.rget(0))

def test_directionallist_remove():
    mlist = DirectionalList()
    mlist.insert(0, 5)
    mlist.insert(0, 4)
    mlist.insert(0, 3)
    mlist.insert(0, 2)
    mlist.insert(0, 1)
    assert(1 == mlist.remove(0))
    assert(5 == mlist.rremove(0))
    assert(3 == mlist.size)

def test_directionallist_revert():
    mlist = DirectionalList()
    mlist.insert(0, 5)
    mlist.rinsert(0, 10)
    assert(2 == mlist.size)
    assert(10 == mlist.rremove(0))
    assert(1 == mlist.size)
    assert(5 == mlist.rget(0))
