from .stack import stack

def test_stack_push_and_pop():
    collection = stack()
    collection.push(10)
    collection.push(11)
    collection.push(12)

    assert collection.pop() == 12
    assert collection.pop() == 11
    assert collection.pop() == 10
    assert collection.is_empty()
