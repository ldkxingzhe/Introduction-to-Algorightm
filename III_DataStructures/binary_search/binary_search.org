#+TITLE: Binary Search Tree 搜索二叉树

搜索树数据结构支持许多动态的集合操作, 包括SEARCH, MINIMUM, MAXIMUM, PREDECESSOR, SUCCESSOR, INSERT, DELETE. 因此可以将搜索树当做一个字典， 或者优先级队列. 

二分树的耗时跟其高成正比。 O(lgn). 我们不能保证， 二分树随机排列， 

* 什么是binary search tree
  搜索二叉树满足一下性质:
Let x be a node in a binary search tree. If y is a node in the left subtree of x, then y.ley <= x.key. If y is a node in the right subtree of x, then y.key >= x.key

二叉树的遍历有三种方式: Inorder tree walk(中序), Preorder tree walk(前序), Post tree walk(后序). 

* 二叉树查询
支持一下查询： MINIMUM, MAXIMUM, SUCCESSOR(接任者， 下一个), PREDECESSOR. 

#+begin_src python
# TREE-SUCCESSOR(x):
  if x.right != NIL
     return TREE-MINIMUM(x.right)
  y = x.p
  while y != NIL AND x == y.right
     x = y
     y = y.p
  return y
#+end_src

* 插值与删除
** 插值
#+begin_src python
TREE-INSERT(T, z)
   y = NIL
   x = T.root
   while x != NIL
      y = x
      if z.key < x.key
         x = x.left
      else x = x.right
   z.p = y
   if y == NIL
      T.root = z       # tree T was empty
   elseif z.key < y.key
      y.left = z
   else y.right = z
#+end_src

** 删除
删除稍微麻烦一点， 分三种情况: 没有后代， 有一个后代， 以及有两个后代. 着重讲述第三种情况: 

如果z有两个后代, 首先找到z的接任者y ---> z的右树, 并代替树中z的位置. z的左树成为y的左树， 这种情况取决于y是否是z的左树. 

#+begin_src python
TRANSPLANT(T, u, v)
   if u.p == NIL
      T.root = v
   elseif u == u.p.left
      u.p.left = v
   else u.p.right = v
   if v != NIL
      v.p = u.p

TREE-DELETE(T, z)
   if z.left == NIL
      TRANSPLANT(T, z, z.right)
   elseif z.right == NIL
      TRANSPLANT(t, z, z.left)
   else y = TREE-MINIMUM(z.right)
      if y.p != z
         TRANSPLANT(T, y, y.right)
         y.right = z.right
         y.right.p = y
      TRANSPLANT(T, z, y)
      y.left = z.left
      y.left.p = y
#+end_src

