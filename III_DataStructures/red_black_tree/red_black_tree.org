#+TITLE: 红黑树

红黑树是众多搜索二叉树中相对平衡的， 可以保证最差情况下， 集合的而动态操作耗时为O(lg n). 

* 红黑树的属性
红黑树是一种搜索二叉树， 它的每一个节点有一个而额外的属性： color, 是RED OR BLACK。 从root节点到叶节点的路径被称为一个简单路径， 红黑树确保， 任意个简单路径不会比其他简单路径长度的二倍还要长。 所以红黑树接近平衡。  

将NILS视为一个指向叶子的指针(外部的节点). 

- 每一个节点不是红色的， 就是黑色的
- 根节点是黑色的
- 每一个叶子(NIL)是黑色的
- 如果一个节点是红色的， 那么它的两个儿子都是黑色的
- 对于每一个节点， 从当前节点出发的到叶子的简单路径包含相同的黑色节点

节点的红黑树的高度: 从当前节点触发到叶节点的路径中出现的黑色节点数， 不包括当前节点。 用bh(x)来表示. 

*引理:* 一个含有n个内部节点的红黑树的高度最多为 2lg(n + 1)

证明这个引理， 首先证明节点x，至少有2^(bh(x)) - 1 个内部节点. 根据属性4(红色节点的儿子都是黑色的)

这样可以导出h <= 2lg(n + 1)

* Rotations(红黑树的旋转)
TREE-INSERT TREE-DELETE两个操作， 将修改红黑树， 这可能会违反红黑树的属性， 必须改变一些节点的颜色， 才能保持红黑树的属性。 

旋转分为左旋与右旋两种。 
#+begin_src python
LEFT-ROTATE(T, x)
    y = x.right    // set y
    x.right = y.left     // turn y'sleft subtree into x's right subtree
    if y.left != T.nil
         y.left.p = x
    y.p = x.p
    if x.p == T.nil
       T.root = y
    elseif x == x.p.left
       x.p.left = y
    esle x.p.right = y
    y.left = x
    x.p = y
#+end_src

* 插入
它的插入遵循搜索二叉树的插入方法. 只是在插入完成后需要进行修复使其满足红黑树的属性. 

新插入的节点初始颜色是红色的. 

需要声明， 算法导论中这个红黑树插入的算法打印错误了， 正确的结构是:
#+begin_src python
RB-INSERT-FIXUP(T, z)
    while z.p.color == RED
        if z.p == z.p.p.left
           y = z.p.p.right
           if y.color == RED
              z.p.color = BLACK
              y.color = BLACK
              z.p.p.color = RED
              z = z.p.p
           else if z == z.p.right
              z = z.p
              LEFT-ROTATE(T, z)
           else
              z.p.color = BLACK
              z.p.p.color = RED
              RIGHT-ROTATE(T, z.p.p)
    T.root.color = BLACK
#+end_src

流程图位于算法导论原版第三版 317页
* 节点删除
时间复杂度为 O(lgn). 吐槽一下，感觉红黑树好晦涩难懂, 特别是算法导论中。 看完一遍云里雾里的。  

** 算法导论的假码
先上所有的假码:
#+begin_src python
RB-TRANSPLANT(T, u, v)
   if u.p == T.nil
      T.root = v
   elseif u == u.p.left
      u.p.left = v
   else
      u.p.right = v
v.p = u.p
#+end_src

#+begin_src python
RB-DELETE(T, z)
    y = z
    y-original-color = y.color
    if z.left == T.nil
       x = z.right
       RB-TRANSLANT(T, z, z.right)
    elseif z.right == T.nil
       x = z.left
       RB-TRANSLANT(T, z, z.left)
    else
       y = TREE-MINIMUM(z.right)
       y-original-color = y.color
       x = y.right
       if y.p == z
          x.p = y
       else
          RB-TRANSLANT(T, y, y.right)
          y.right = z.right
          y.right.p = y
       RB-TRANSPLANT(T, z, y)
       y.left = z.left
       y.left.p = y
       y.color = z.color
    if y-original-color == BLACK
       RB-DELETE-FIXUP(T, x)
#+end_src


#+begin_src python
RB-DELETE-FIXUP(T, x)
   while x != T.root and x.color == BLACK
      if x == x.p.left
         w = x.p.right
         if w.color == RED
            w.color = BLACK
            x.p.color = RED
            LEFT-ROTATE(T, x.p)
            w = x.p.right
      if w.left.color == BLACK and w.right.color == BLACK
         w.color = RED
         x = x.p
      else if w.right.color == BLACK
         w.left.color = BLACK
         w.color = RED
         RIGHT-ROTATE(T, w)
         w = x.p.right
         
         w.color = x.p.color
         x.p.color = BLACK
         w.right.color = BLACK
         LEFT-ROTATE(T, x.p)
         x = T.root
      else (same as then cause with "right" and "left" exchanged)

x.color = BLACK
     
#+end_src

** 来自GeekForgeeks的博客
这篇博客的讲解方式比起算法导论中而言更加通俗易懂， 看完之后瞬间清楚了. 
http://www.geeksforgeeks.org/red-black-tree-set-3-delete-2/

红黑树的插入操作中， 我们检查uncle的颜色决定对应的case, 删除操作中则检查兄弟的颜色. 

插入操作中， 主要违反的属性是两个连续的红色节点. 而删除操作， 违反的主要属性是, 节点的高度会发生改变. 

删除操作是一个相当复杂的操作， 为了便于理解， 引入: double black. 当一个黑节点被删除， 而且被一个黑儿子替换， 则这个黑儿子被标记为double black. 主要任务变成了， 如何将double black节点转化为 single black. 

*** 标准的BST删除(标准的搜索二叉树删除)
let v = 被删除的节点
let u = 删除节点的接任者(可以为null)

在标准的BST删除中， 最终要处理的情况都是删除一个叶节点或者是只有一个儿子的节点. 对于中间的节点, 我们可以先复制交换接任者， 最后将原有的接任者删除， 这样情况又回到了只删除一个叶节点或者只有一个儿子节点的情况. 
*** 简单情况: 如果u或者v是红色的
这时将接任者u的颜色设置为黑色
[[http://www.geeksforgeeks.org/wp-content/uploads/rbdelete11.png]]
*** 如果u和v都是黑色的
*将u标记为double black(双重黑色)* 
  [[http://www.geeksforgeeks.org/wp-content/uploads/rbdelete12_new.png]]

*如果当前节点u是double black而且不是根节点， 一直进行循环, let s=u的兄弟节点*

- ...(a): 如果s是黑色的， 而且s的孩子至少有一个是红色的. 
- ........ (i  ) Left Left Case(s 是它父亲的左儿子, r是s的左儿子， 或者是s的两个儿子都是红色的)
- ........ (ii ) Left Right Case(s是它父亲的左儿子, r是s的右儿子)
- ........ (iii) Right Right Case(s是它父亲的右儿子, r是s的右儿子, 或者s的两个儿子都是红色的) [[http://www.geeksforgeeks.org/wp-content/uploads/rbdelete13New.png][例子]]
- ........ (iv ) Right Left Case (s是它父亲的右儿子， r是s的左儿子) [[http://www.geeksforgeeks.org/wp-content/uploads/rbdelete14.png][例子]]
- ...(b): 如果s是黑色的, 而且两个儿子也是黑色的, 此时s需要变成红色并且遍历循环. [[http://www.geeksforgeeks.org/wp-content/uploads/rbdelete15.png][例子]]
- ...(c): 如果s是红色的, 通过旋转是s提升, 改变s的颜色与p的颜色. 新的兄弟总是黑色的 [[http://www.geeksforgeeks.org/wp-content/uploads/rbdelete161.png][例子]]
- ........ (i  ) Left Case(s是它父亲的左儿子)
- ........ (ii ) Right Case(s是它父亲的右儿子)

u = p

*如果U是根节点， 将变成黑色的结束*

