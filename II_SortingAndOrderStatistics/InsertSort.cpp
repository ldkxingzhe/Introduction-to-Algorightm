#include<iostream>

void sort(int*,int);
using namespace std;
int main(int argc, char *argv[])
{
    int arr[] = {1,2,5,1,2,4,7,4,32,56,32};
	cout << "Old arr = " << endl;
	for(int a: arr){
		cout << a << "\t";
	}
	cout << endl;
	sort(arr,sizeof(arr)/sizeof(int));
	for(int a: arr){
		cout << a << "\t";
	}
	cout << endl;
    return 0;
}

void sort(int *arr,int len){
	for(int i = 1; i < len; ++i){
		int tmp = arr[i];
		for(int j = i - 1; j >= 0; --j){
			if(tmp < arr[j]){
				arr[j+1] = arr[j];
			}else{
				arr[j+1] = tmp;
				break;
			}
		}
	}
}
