def largest_sum_contiguous_subarray(array):
    best_start = 0
    best_end = 0
    best_sum = array[0]
    current_start = 0
    current_sum = best_sum
    
    for i in range(1, len(array)):
        current = array[i]
        if current_sum < 0:
            current_start = i
            current_sum = current
        else:
            current_sum += current

        if current_sum > best_sum:
            best_start = current_start
            best_end = i
            best_sum = current_sum

    return best_start, best_end, best_sum


if __name__ == '__main__':
    array = [-2, -3, 4, -1, -2, 1, 5, -3]
    best_start, best_end, best_sum = largest_sum_contiguous_subarray(array)
    print((best_start, best_end, best_sum))
    assert best_start == 2
    assert best_end == 6
    assert best_sum == 7
