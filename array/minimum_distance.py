def minimum_distance(array, x, y):
    mini_index_x = None
    mini_index_y = None

    best_x = None
    best_y = None
    best_distance = None

    for index in range(len(array)):
        current = array[index]
        distance = None
        if current == x:
            mini_index_x = index
            if mini_index_y is not None:
                distance = index - mini_index_y
        elif current == y:
            mini_index_y = index
            if mini_index_x is not None:
                distance = index - mini_index_x

        if distance is not None:
            if best_distance is None or distance < best_distance:
                best_x = mini_index_x
                best_y = mini_index_y
                best_distance = distance

    return best_x, best_y, best_distance


if __name__ == '__main__':
    array = [3, 5, 4, 2, 6, 5, 6, 6, 5, 4, 8, 3]
    x = 3
    y = 6
    best_x, best_y, best_distance = minimum_distance(array, x, y)
    print("BestX: ", best_x, "BestY: ", best_y, ", BestDistance: ", best_distance)
