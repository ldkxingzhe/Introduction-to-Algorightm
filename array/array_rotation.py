def array_rotation(array, index):
    for index in range(index + 1):
        zero = array[0]
        for i in range(len(array) - 1):
            array[i] = array[i + 1]
        array[len(array) - 1] = zero


if __name__ == '__main__':
    array = [1, 2, 3, 4, 5, 6, 7]
    array_rotation(array, 1)
    assert array[0] == 3
    assert array[6] == 2
    print("array is: ", array)
