#!/usr/bin/env python3

"""
Given a string containing digits from 2-9 inclusive, return all possible letter combinations 
that the number could represent.
"""

class Solution:
    def letter_combinations(self, digits):
        if digits is None or len(digits) == 0:
            return []
        maps = {
            '2': ['a', 'b', 'c'],
            '3': ['d', 'e', 'f'],
            '4': ['g', 'h', 'i'],
            '5': ['j', 'k', 'l'],
            '6': ['m', 'n', 'o'],
            '7': ['p', 'q', 'r', 's'],
            '8': ['t', 'u', 'v'],
            '9': ['w', 'x', 'y', 'z']
        }
        if len(digits) == 1:
            return maps[digits[0]]
        cases = self.letter_combinations(digits[1:])
        return [ch + case for ch in maps[digits[0]] for case in cases]

if __name__ == '__main__':
    solution = Solution()
    result = solution.letter_combinations("23")
    print(f'result: {result}')
