#!/usr/bin/env python3

"""
Given a set of distinct integers, nums, return all possible subsets (the power set)

Note: The solution set must not contain duplicate subsets.
"""

class Solution:
    def subsets(self, nums):
        result = [[]]
        for case in self._subsets(nums, 0):
            if len(case) == 0:
                continue
            else:
                result.append(case)
        return result

    def _subsets(self, nums, start_index):
        if start_index == len(nums):
            return
        yield [nums[start_index]]
        for case in self._subsets(nums, start_index + 1):
            yield case
            another_case = list(case)
            another_case.insert(0, nums[start_index])
            yield another_case


if __name__ == '__main__':
    s = Solution()
    result = s.subsets([1, 2, 3])
    print(f'result: {result}')
