#!/usr/bin/env python3

"""
There are two sorted arrays nums1 and nums2 of size m and n respectively. 
Find the median of the two sorted arrays. The overall run time complexity should be O(log(m + n)
You may assume nums1 and nums2 cannot be both empty
"""

def find_median_sorted_arrays(nums1, nums2):
    total = len(nums1) + len(nums2)
    is_odd = True if total % 2  != 0 else False
    if len(nums1) > len(nums2):
        longer_nums = nums1
        shorter_nums = nums2
    else:
        longer_nums = nums2
        shorter_nums = nums1
    middle_left = int(total / 2)
    scope = (0, len(shorter_nums))     # 前middle_left中， shorter_num中可能包含的值范围
    while True:
        shorter_middle_num = int((scope[0] + scope[1]) / 2)
        longer_num = middle_left - shorter_middle_num
        if shorter_middle_num > 0 and shorter_nums[shorter_middle_num - 1] > longer_nums[longer_nums]:
            pass
        
        
if __name__ == '__main__':
    nums1 = [1, 3, 5, 6, 7, 9, 100]
    nums2 = [2, 10, 12, 39, 40]
    print(str(find_median_sorted_arrays(nums1, nums2)))
    

    5, 6, 7, 9, 10, 12
