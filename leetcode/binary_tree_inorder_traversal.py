class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right= None


class Solution:
    def inorder_traversal(self, root):
        result = []
        if root:
            self._inorder_traversal(root, result)
        return result


    def _inorder_traversal(self, root, result):
        if root.left:
            self._inorder_traversal(root.left, result)
        result.append(root.val)
        if root.right:
            self._inorder_traversal(root.right, result)



if __name__ == '__main__':
    root = TreeNode(1)
    root.right = TreeNode(2)
    root.right.left = TreeNode(3)
    s = Solution()
    assert [1, 3, 2] == s.inorder_traversal(root)
