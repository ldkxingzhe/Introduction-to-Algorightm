#!/usr/bin/env python3

"""
Given a binary tree, determine if it is valid binary search tree (BST).
"""

class Solution:
    def isValidBST(self, root):
        last_val = None
        for case in self.middle_order(root):
            if last_val or last_val < case:
                last_val= case
                continue
            return False
        return True


    def middle_order(self, root):
        if root is None:
            return
        for case in self.middle_order(root.left):
            yield case
        yield root.val
        for case in self.middle_order(root.right):
            yield case
