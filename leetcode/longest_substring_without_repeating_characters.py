#!/usr/bin/env python3

"""
Given a string, find the length of the longest substring without repeating characters.
"""


def length_of_longest_substring(string):
    diff = {}
    largest = None # (size, start, end) [start, end)
    current_start = 0
    for index, val in enumerate(string):
        if val not in diff:
            diff[val] = index
            continue
        current_largest = index - current_start
        print(f'val : {val}')
        if largest is None or current_largest > largest[0]:
            largest = (current_largest, current_start, index)
        found_index = diff[val]
        for out_date_val in string[current_start:found_index + 1]:
            if out_date_val in diff:
                del diff[out_date_val]
        current_start = found_index + 1
        diff[val] = index

    if largest is None or len(string) - current_start > largest[0]:
        largest = (len(string) - current_start, current_start, len(string))
    return string[largest[1]: largest[2]]

if __name__ == '__main__':
    print(length_of_longest_substring('babad'))
