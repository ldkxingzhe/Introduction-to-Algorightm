#!/usr/bin/env python3

"""
Given an array of integers, return indices of the two number such that they add up to a specific target.
"""


def two_sum(nums, target):
    value_2_index = { value: index for index, value in enumerate(nums)  }
    for index, val in enumerate(nums):
        remain = target - val
        if remain in value_2_index and index != value_2_index[remain]:
            return [index, value_2_index[remain]]

    return None


if __name__ == '__main__':
    values = [2, 4, 8, 10, 20]
    print(str(two_sum(values, 18)))
