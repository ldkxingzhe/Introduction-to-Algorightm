#!/usr/bin/env python3

"""
Given an array of non-negative integers, you are initially positioned at the first index of 
the array. Each element in the represents your maximum jump length at that position

Input: [2, 3, 1, 1, 4]
Output: true
"""

class Solution:
    def canJump(self, nums):
        zero = []
        for index, val in enumerate(nums):
            if val == 0:
                zero.append(index)
        if len(zero) == 0:
            return True
        for index, val in enumerate(nums):
            target_index = index + val
            if index == len(nums) - 1:
                return True
            while len(zero) > 0:
                zero_case = zero[0]
                if index == zero_case:
                    return False
                if index < zero_case and target_index > zero_case:
                    zero.remove(zero_case)
                else:
                    break
            if len(zero) == 0:
                return True
        

if __name__ == '__main__':
    s = Solution()
    assert s.canJump([2, 0, 0])
    assert s.canJump([0])
    assert not s.canJump([3, 2, 1, 0, 4])
    assert s.canJump([2, 3, 1, 1, 4])
