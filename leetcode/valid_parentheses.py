#!/usr/bin/env python3

"""
Given a string containing just the characters '(',')', '{', '}', '[' and ']', determine if the input string
is valid: 

An input string is valid if:
- 1. Open brackets must be closed by the same type of brackets.
- 2. Open brackets must be closed in the correct order.
"""


class Solution:
    def is_valid(self, chars):
        parentheses = []
        parentheses_map ={')': '(', '}': '{', ']': '['}
        for ch in chars:
            if ch in parentheses_map:
                if len(parentheses) == 0 or parentheses[-1] != parentheses_map[ch]:
                    return False
                else:
                    parentheses = parentheses[:-1]
            else:
                parentheses.append(ch)
        return len(parentheses) == 0

if __name__ == '__main__':
    solution = Solution()
    assert solution.is_valid('()')
    assert solution.is_valid('()[]{}')
    assert not solution.is_valid('(]')
    assert not solution.is_valid('([)]')
