#!/usr/bin/env python3

class Solution:
    def largest_rectangle_area(self, heights):
        largest_num = 0
        for index, height in enumerate(heights):
            before = self.num_before_smaller(heights, index)
            area = (index - before) * height
            if area > largest_num:
                largest_num = area
        print(f'largest_num: {largest_num}')
        return largest_num

    def num_before_smaller(self, heights, current_index):
        for index in range(current_index - 1, -1, -1):
            if heights[index] < heights[current_index]:
                return index
        return -1


if __name__ == '__main__':
    heights = [2, 1, 5, 6, 2, 3]
    s = Solution()
    assert 10 == s.largest_rectangle_area(heights)
