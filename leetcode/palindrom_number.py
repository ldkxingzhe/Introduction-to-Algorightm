#!/usr/bin/env python3

"""
Determine whether an integer is a palindrome. An integer is palindrome when it reads the same backward as forward.

Input: 121
Output: true

Input: -121
Output: False

Input: 10
Output: false
"""

class Solution:
    def is_palindrom(self, x: int) -> bool:
        if x < 0:
            return False
        value = str(x)
        start, end = 0, len(value) - 1
        while start < end:
            if value[start] != value[end]:
                return False
            start += 1
            end -= 1
        return True


if __name__ == '__main__':
    solution = Solution()
    assert solution.is_palindrom(11)
    assert solution.is_palindrom(121)
    assert not solution.is_palindrom(-121)
    assert not solution.is_palindrom(10)
