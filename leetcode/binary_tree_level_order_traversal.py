#!/usr/bin/env python3

"""
Given a binary tree, return the level order traversal of it's node's values (from left to right, level by level)
"""

class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None



class Solution:
    def level_order(self, root):
        result = []
        self._level_order(root, result, 0)
        return result


    def _level_order(self, node, result, height):
        if node is None:
            return
        if height < len(result):
            level_array = result[height]
        else:
            level_array = []
            result.append(level_array)
        level_array.append(node.val)
        self._level_order(node.left, result, height + 1)
        self._level_order(node.right, result, height + 1)



