#!/usr/bin/env python3


class Solution:
    def unique_path_with_obstacles(self, grid):
        y = len(grid)
        if y == 0:
            return 0
        x = len(grid[0])
        for y_index in range(y - 1, -1, -1):
            row = grid[y_index]
            for x_index in range(x - 1, -1, -1):
                current = row[x_index]
                if current == 1:
                    row[x_index] = 0
                elif y_index == y - 1 and x_index == x - 1:
                    row[x_index] = 1
                else:
                    if y_index == y - 1:
                        current_down = 0
                    else:
                        current_down = grid[y_index + 1][x_index]
                    if x_index == x - 1:
                        current_right = 0
                    else:
                        current_right = grid[y_index][x_index + 1]
                    row[x_index] = current_down + current_right

        return grid[0][0]
        

if __name__ == '__main__':
    s = Solution()
    input = [
        [0, 0, 0],
        [0, 1, 0],
        [0, 0, 0]
    ]
    result = s.unique_path_with_obstacles(input)
    print(str(result))
    print(str(input))
    
