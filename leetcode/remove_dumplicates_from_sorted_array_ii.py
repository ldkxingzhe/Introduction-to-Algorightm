#!/usr/bin/env python3

"""
Given a sorted array nums, remove the duplicates in-place such that duplicates appeared at most twice 
and return the new length.
"""

class Solution:
    def remove_dumplicates(self, nums):
        swap_index = 0
        last_num = None
        last_num_times = 1
        for index, num in enumerate(nums):
            if last_num == num:
                last_num_times += 1
            else:
                last_num_times = 1
            last_num = num
            if last_num_times >= 3:
                continue
            if index != swap_index:
                # swap_index与index位置不同, 交换位置
                nums[swap_index], nums[index] = nums[index], nums[swap_index]
            swap_index += 1

        return swap_index


if __name__ == '__main__':
    s = Solution()
    nums = [0, 0, 1, 1, 1, 2, 2, 3]
    result = s.remove_dumplicates(nums)
    print(f'result index: {result}, and nums: {nums}')
