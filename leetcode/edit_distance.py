#!/usr/bin/env python3

"""
Given two words word1 and word2, find the minimum number of operations required to convert word1 to word2. 

You have the following 3 operations permitted on a word:
- Insert a character
- Delete a character
- Replace a character
"""

class Solution:
    def min_distance(self, word1, word2):
        m = len(word1)
        n = len(word2)
        dp = [0] * (m + 1)
        for i in range(m + 1):
            dp[i] = [0] * (n + 1)
            dp[i][0] = i
        for j in range(n + 1):
            dp[0][j] = j
        for i in range(1, m + 1):
            for j in range(1, n + 1):
                if word1[i - 1] == word2[j - 1]:
                    dp[i][j] = dp[i - 1][j - 1]
                else:
                    dp[i][j] = min(dp[i - 1][j - 1], dp[i][j - 1], dp[i - 1][j]) + 1
        return dp[m][n]

if __name__ == '__main__':
    s = Solution()
    assert 3 == s.min_distance('horse', 'ros')
