#!/usr/bin/env python3

"""
Validate if a given string can be interpreted as a decimal number.

Some examples:
'0' => true
'   0.1  ' => true
'abc' => false
'1 a' => false
'2e10' => true
'-90e3' => true
'  1e ' => false
'e3' => false
'6e-1' => true
'99e2.5' => false
'53.5e93' => true
'--6' => false
'-+3' => false
'95a54e53' => false
"""

class Solution:
    nums = {str(x) for x in range(10)}

    def is_number(self, s):
        e = False
        sign = False
        point = False
        for ch in s:
            if ch == ' ':
                continue
            if ch == '-' or ch == '+':
                if sign:
                    return False
                else:
                    sign = True
            elif ch in Solution.nums:
                sign = True
            elif ch == '.':
                if e or point:
                    return False
                point = True
            elif ch == 'e' or ch == 'E':
                if e or not sign:
                    return False
                e = True
                sign = False
                point = False
            else:
                return False
        
        return sign


if __name__ == '__main__':
    s = Solution()
    assert s.is_number('.1')
    assert s.is_number("0")
    assert s.is_number("  0.1")
    assert not s.is_number("abc")
    assert not s.is_number("1 a")
    assert s.is_number("2e10")
    assert s.is_number(" -90e3")
    assert s.is_number("1e3")
    assert not s.is_number(" 1e")
    assert not s.is_number("e3")
    assert not s.is_number("-+3")
    assert not s.is_number("95a54e53")
    assert not s.is_number('.')
