#!/usr/bin/env

"""
Given a binary tree, determine if it is a valid binary search tree (BST).

"""

class TreeNode:
    def __init__(self, x):
        self.val = x
        
