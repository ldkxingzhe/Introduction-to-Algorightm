#!/usr/bin/env python3

"""
Given a 32-bit signed integer, reverse digits of an integer.
"""

def reverse(value):
    negative = value < 0
    if negative:
        value = - value
    digits = []
    while value > 0:
        remainder = value % 10
        digits.append(remainder)
        value = int((value - remainder) / 10)
    result = 0
    length = len(digits)
    current = 1
    for index in range(length - 1, -1, -1):
        item = digits[index]
        result += item * current
        current *= 10
    if n > 2**31:
        return 0
    if negative:
        result = -result
    return result


if __name__ == '__main__':
    assert 321 == reverse(123)
    assert -321 == reverse(-123)
    assert 21 == reverse(120)
