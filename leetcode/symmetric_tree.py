#!/usr/bin/env python3

"""
Given a binary tree, check whether it is a mirror of itself
"""

class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None


class Solution:
    def is_symmetric(self, root):
        if root is None:
            return True
        return self.is_symmetric_tree(root.left, root.right)


    def is_symmetric_tree(self, left, right):
        if left == right:
            return True
        if left is None or right is None:
            return False
        return left.val == right.val \
            and self.is_symmetric_tree(left.left, right.right) \
            and self.is_symmetric_tree(left.right, right.left)
