#!/usr/bin/env python3

"""
Given a binary tree, flatten it to a linked list in-place
https://leetcode.com/problems/flatten-binary-tree-to-linked-list/
"""

class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

    def pre_traversal(self):
        print(f"{self.val}", end=' ')
        if self.left:
            self.left.pre_traversal()
        else:
            self.right.pre_traversal()


class Solution:
    def flatten(self, root):
        self._flatten(root, None)

    def _flatten(self, current_root, right):
        _right = None
        if current_root.right is not None:
            self._flatten(current_root.right, right)
            _right = current_root.right
        else:
            _right = right

        if current_root.left is not None:
            self
            current_root.right = current_root.left
            current_root.left = None
    

if __name__ == '__main__':
    pass
