#!/usr/bin/env python3

"""
Given n pairs of parentheses, write a function to generate all combinations of well-formed parentheses.
"""


class Solution:
    def generate_parenthesis(self, n):
        return self.possible_cases(n, n)

    def possible_cases(self, left_left, right_left):
        if left_left == 0 and right_left == 0:
            return ['']
        result = []
        if left_left >= 1:            
            for item in self.possible_cases(left_left - 1, right_left):
                result.append('(' + item)
        if right_left > left_left:
            for item in self.possible_cases(left_left, right_left - 1):
                result.append(')' + item)
        return result

if __name__ == '__main__':
    solution = Solution()
    result = solution.generate_parenthesis(3)
    print(str(result))
